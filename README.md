Dynamic dns service with [Bind](https://www.isc.org/downloads/bind/).

# Volumes
- /etc/bind/zones/

# Environment Variables
## DDNS_DOMAINS

A semicolon seperated list of dynamic domains. Each Item is split by colon.
The first part is the domain, the second part the secret.

## DDNS_DOMAIN
The base domain.

# Ports
- 80
