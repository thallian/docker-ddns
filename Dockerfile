FROM registry.gitlab.com/thallian/docker-confd-env:master

RUN apk add --no-cache fcgiwrap nginx bind bind-tools perl perl-cgi

RUN rm /etc/nginx/conf.d/default.conf

ADD /rootfs /

RUN mkdir /run/nginx
RUN mkdir /var/run/fcgiwrap

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

RUN chown nginx /run/nginx
RUN chown nginx /var/run/fcgiwrap
RUN chown nginx /var/lib/ddns

EXPOSE 80
VOLUME "/etc/bind/zones/"
